-- =============================================
-- VJ 012/17/2025 DTE-1722 Ensure duplicate 14.0 CDL claims files aren't loaded to SSRS. If found duplicate files, then don't process and email
-- =============================================
CREATE  OR ALTER  PROCEDURE [dbo].[spCDLClaimsProcessing_EmailForMultipleFiles] ( @reportDir  nvarchar(255), @fileCnt int,  @emailList  nvarchar(2000))
AS  
BEGIN
  
	declare @subject nvarchar(255)='CDL Claims Detail File Processing Error at server ' ++@@SERVERNAME + '.'
	declare @emailBody nvarchar(255)= convert(nvarchar(55),@fileCnt) + ' files found at directory' + @reportDir+ '. Please review files and delete extra files from directory and ask APP Dev Team to run job again.'

	begin
		declare @emailErrorCode int
		
		EXEC @emailErrorCode= msdb.dbo.sp_send_dbmail
		@recipients = @emailList
		, @subject =@subject
		, @body = @emailBody
		, @body_format  = 'TEXT'
		, @profile_name = 'SQLAlerts';
		
	end
END
GO

